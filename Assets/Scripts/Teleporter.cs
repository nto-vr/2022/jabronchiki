using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] private Transform _testCube;
    [SerializeField] private float _timeBetweenPoints;
    private List<Vector3> _points = new List<Vector3>();
    private bool _canStartTp = true;
    private bool _canTp;

    public void TpCast(Vector3 origin, Vector3 vector, float height=2, float delta=0.1f, int maxiter=100)
    {
        if (!_canStartTp)
            return;
        
        RaycastHit hit;
        bool foundHit = false;
        _points = new List<Vector3>();
        for(int i=0; i < maxiter; i++)
        {
            _points.Add(origin);
            if(Physics.Raycast(origin, vector, out hit, vector.magnitude * delta))
            {
                foundHit = true;
                origin = hit.point;
                _points.Add(origin);
                break;
            }
            origin += vector * delta;
            vector.y += 0.025f * Physics.gravity.y * delta;
        }

        _canTp = foundHit && !Physics.Raycast(origin, new Vector3(0, 1, 0), out hit, height);

        if (!_canTp) 
            return;
        
        _testCube.position = _points[_points.Count - 1] + new Vector3(0, 0.1f, 0);
    }
    
    public void TpBegin()
    {
        if (!_canStartTp)
            return;
        
        _testCube.gameObject.SetActive(true);
        _testCube.rotation = Quaternion.Euler(45, 0, 45);
    }

    public void TpEnd()
    {
        if (_canTp && _canStartTp)
            StartCoroutine(TpAnimation());
    }

    private IEnumerator TpAnimation()
    {
        _canStartTp = false;
        var rot = Quaternion.Euler(new Vector3(Random.Range(5, 15), Random.Range(5, 15), Random.Range(5, 15)));
        _testCube.rotation = rot;

        for (var i = 1; i < _points.Count; i++)
        {
            _testCube.position = _points[i - 1];
            var timeElapsed = 0f;
            
            while (timeElapsed < _timeBetweenPoints)
            {
                _testCube.position = Vector3.Lerp(_points[i - 1], _points[i], timeElapsed / _timeBetweenPoints);
                _testCube.rotation *= rot;
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
        
        _testCube.gameObject.SetActive(false);
        gameObject.transform.position = _points[_points.Count - 1] + new Vector3(0, 1f, 0);
        _canStartTp = true;
    }
}
