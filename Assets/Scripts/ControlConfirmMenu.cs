using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlConfirmMenu : MonoBehaviour
{
    [SerializeField] private GameObject MenuWindow;

    public void OpenConfirmWindow()
    {
        MenuWindow.SetActive(false);
        gameObject.SetActive(true); // open confirm window
    }
    public void CloseConfirmWindow()
    {
        gameObject.SetActive(false);
        MenuWindow.SetActive(true);
    }
    public void QuitGame() => Application.Quit(); // close game
}
