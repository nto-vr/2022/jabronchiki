using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Mover : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _rotateSpeed;
    private Rigidbody _rigidbody;
    private Vector2 _velocity;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    private void Update()
    {
        var rbVelocity = _rigidbody.velocity;
        var velocity = transform.rotation * new Vector3(_velocity.x, rbVelocity.y, _velocity.y);
        _rigidbody.velocity = velocity;

        //var rbVelocity = _rigidbody.velocity;
        //var velocity = new Vector3(_velocity.x, rbVelocity.y, _velocity.y);
        //var k = Mathf.Abs(rbVelocity.magnitude - _velocity.magnitude);
        //_rigidbody.AddRelativeForce(velocity * (k * 10 / _speed));
    }

    public void SetVelocity(Vector2 newVelocity)
    {
        _velocity = newVelocity.normalized * _speed;
    }

    public void Rotate(float y)
    {
        transform.eulerAngles += new Vector3(0, y * _rotateSpeed, 0);
    }
}
