using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    public GameObject[] asteroids;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            Instantiate(asteroids[Random.Range(0, asteroids.Length)], new Vector3(Random.Range(-100, 100), Random.Range(-20, 20), -300), new Quaternion(0, 0, 0, 0)).GetComponent<Rigidbody>().velocity = new Vector3(0, 0, Random.Range(2, 10));
            yield return new WaitForSeconds(0.5f);
        }
    }

    // Update is called once per frame
}
