using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sirena : MonoBehaviour
{
    public GameObject light;
    public AudioSource audio;
    bool inn = false;
    private void OnTriggerStay(Collider collision)
    {
        inn = true;
    }
    private void OnTriggerExit(Collider collision)
    {
        inn = false;
    }
    private void Start()
    {
        StartCoroutine("Blink");
    }
    IEnumerator Blink()
    {
        while (true)
        {
            if (inn)
            {
                light.SetActive(true);
                yield return new WaitForSeconds(0.2f);
                light.SetActive(false);
                audio.enabled = true;
                
            }
            else
            {
                audio.enabled = false;
            }
            yield return new WaitForSeconds(0.2f);
        }
    }

}
