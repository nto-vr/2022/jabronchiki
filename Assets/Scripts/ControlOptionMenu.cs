using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlOptionMenu : MonoBehaviour
{
    [SerializeField] private GameObject MenuWindow;

    public void Open()
    {
        MenuWindow.SetActive(false);
        gameObject.SetActive(true);
    }
    public void BackToMenu()
    {
        gameObject.SetActive(false);
        MenuWindow.SetActive(true);
    }
}
