using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    public bool timer;
    int time=0;
    // Update is called once per frame
    void FixedUpdate()
    {
        if(gameObject.transform.position.z >= 50)
        {
            Destroy(gameObject);
        }
        if (timer)
        {
            time++;
            if (time >= 200)
            {
                Destroy(gameObject);
            }
        }
    }
}
