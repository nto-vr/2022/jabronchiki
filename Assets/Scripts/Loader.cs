using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    [SerializeField] private Canvas _canvas;
    private Slider _slider;
    
    [SerializeField] private float _loadDuration;
    [SerializeField] private float _hideDuration;
    private float _timeElapsed;

    private void Awake()
    {
        _slider = _canvas.GetComponentInChildren<Slider>();
        
        StartCoroutine(Load());
    }

    private IEnumerator Load()
    {
        while (_timeElapsed < _loadDuration)
        {
            _timeElapsed += Time.deltaTime;
            _slider.value = _timeElapsed / _loadDuration;
            yield return null;
        }
        
        _timeElapsed = 0;
        StartCoroutine(FinishLoading());
    }

    private IEnumerator FinishLoading()
    {
        var canvasGroup = _canvas.GetComponent<CanvasGroup>();

        while (_timeElapsed < _hideDuration)
        {
            _timeElapsed += Time.deltaTime;
            canvasGroup.alpha = 1 - _timeElapsed / _hideDuration;
            yield return null;
        }
    }
}
