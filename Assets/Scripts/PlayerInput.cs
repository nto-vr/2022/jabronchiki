using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private Transform _leftHand;
    private Mover _mover;
    private Teleporter _teleporter;
    
    private void Awake()
    {
        _mover = _player.GetComponent<Mover>();
        _teleporter = _player.GetComponent<Teleporter>();
    }

    private void Update()
    {
        var velocity = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
        _mover.SetVelocity(new Vector2(velocity.y, -velocity.x));
        
        _mover.Rotate(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x);
        
        if (OVRInput.Get(OVRInput.RawButton.Y))
            _teleporter.TpCast(_leftHand.position, _leftHand.forward);
        
        if (OVRInput.GetDown(OVRInput.RawButton.Y))
            _teleporter.TpBegin();
        
        if (OVRInput.GetUp(OVRInput.RawButton.Y))
            _teleporter.TpEnd();

        if (OVRInput.GetDown(OVRInput.Button.Start))
            _mover.gameObject.transform.position = new Vector3(0, 1, 0);
    }
}
