using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlVolume : MonoBehaviour
{
    [SerializeField] private Text valueTxt;
    private AudioSource audioSource;

    public float volumeValue = 0.5f;

    private void Awake()
    {
        audioSource.Play();
    }
    public void DecreaseVolume()
    {
        volumeValue = Mathf.Max(volumeValue - 0.1f, 0);
        valueTxt.text = ((int)Mathf.Round(volumeValue * 10)).ToString();
    }
    public void IncreaseVolume()
    {
        volumeValue = Mathf.Min(volumeValue + 0.1f, 1);
        valueTxt.text = ((int)Mathf.Round(volumeValue * 10)).ToString();
    }
}
