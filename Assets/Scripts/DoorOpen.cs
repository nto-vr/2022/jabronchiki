using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    public Animator animator;
    public string id; 
    // Start is called before the first frame update

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        animator.SetBool(id, true);
    }

    void OnTriggerExit(Collider other)
    {
        animator.SetBool(id, false);
    }

}
